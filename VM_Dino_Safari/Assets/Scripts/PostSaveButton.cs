﻿//using UnityEngine;
//using System.Collections;
//using UnityEngine.UI;
//using UnityEngine.EventSystems;
//
////using gametheater;
////
////using DG.Tweening;
//
//public class PostSaveButton : IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {//gtMonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {
//
//	protected PostOverlayController postController;
//	public int index;
//
//	public Sprite BackgroundDefault;
//	public Sprite BackgroundHover;
//	public Sprite BackgroundClick;
//
//	UnityEngine.UI.Image background;
//
//	public GameObject spinner;
//	public GameObject saveIcon;
//	public GameObject saveText;
//
////	public AlphaDecay helpText;
////	public AlphaDecay permText;
////	public AlphaDecay helpText;
//
//	int count;
//	bool lookedAway = false;
//	bool saving = false;
//	bool saved = false;
//
//	bool ready = false;
//
//	int countMax = 60;
//
//	// Use this for initialization
//	void Start ()
//	{
////		postController = FindObjectOfType (typeof(PostOverlayController)) as PostOverlayController;
////
////		background = GetComponent<Image>();
//
//		spinner.SetActive (false);
//		saveText.SetActive (false);
////		helpText.HideText();
////
////		permText.HideText();
//
//#if UNITY_IOS
//		helpText.GetComponent<VMTextFieldLocalizer>().SetKey("gallery_ios_save");
//#elif UNITY_ANDROID
//		helpText.GetComponent<VMTextFieldLocalizer>().SetKey("gallery_android_save");
//#endif
//
//		//saveText.GetComponent<Text>().text = "Save Image";
//		saved = false;
//
////		StartCoroutine(GetReady());
//	}
//
//	IEnumerator GetReady() {
//		yield return new WaitForSeconds(2.0f);
//		ready = true;
//	}
//
//	void Update()
//	{
//		if (count > 0)
//		{
//			spinner.transform.Rotate (0f, 0f, 300f * Time.deltaTime);
//			count--;
//
//			if(count == 0)
//			{
//				spinner.SetActive(false);
//	            saveText.GetComponent<VMTextFieldLocalizer>().SetKey("saved");
//			}
//		}
//
//		if(lookedAway && count == 0)
//		{
//			background.sprite = BackgroundDefault;
//			lookedAway = false;
//		}
//
//		if(saving && count == countMax - 10)
//		{
//			postController.SaveImage(index);
//			saving = false;
//		}
//	}
//
//	public void OnPointerClick(PointerEventData data)
//	{
//		if(ready == false)
//			return;
//
//		// Check camera permissions
//		if(!NativeBridge.IsLibraryAuthorized())
//		{
//			permText.ShowThenHideText();
//
//
//
//			//postController.HideHelp();
//
////			lookatText.HideThenShowText();
//			helpText.HideThenShowText();
//		}
//		else if (!saved)
//		{
//			background.sprite = BackgroundClick;
//			saveIcon.SetActive(false);
//			spinner.SetActive (true);
//			helpText.HideText();
//            saveText.GetComponent<VMTextFieldLocalizer>().SetKey("saving");
//            saving = true;
//			count = countMax;
//			saved = true;
//			SoundsPool.instance.PlayConfirm ();
//		}
//	}
//
//	public void OnPointerEnter(PointerEventData data)
//	{
//		if (!saved) {
//			saveIcon.SetActive (true);
//			background.sprite = BackgroundHover;
//
//			helpText.ShowText();
//
//
//			postController.PhotoLookedAt(index);
//
//			//if(NativeBridge.IsLibraryAuthorized())
////			if(true)
////				postController.PhotoLookedAt(index);
//		}
//		lookedAway = false;
//		saveText.SetActive (true);
//
//	}
//
//	public void OnPointerExit(PointerEventData data)
//	{
////		if(!NativeBridge.IsLibraryAuthorized())
////		if(true)
////			return;
//
//		permText.HideText();
//
//		lookedAway = true;
//		saveIcon.SetActive(false);
//		helpText.HideText();
//
//		postController.PhotoLookedAway(index);
//
//		if (!saved)
//		{
//			saveText.SetActive (false);
//			//if(NativeBridge.IsLibraryAuthorized())
////			if(true)
////				postController.PhotoLookedAway(index);
//		}
//	}
//
//}
