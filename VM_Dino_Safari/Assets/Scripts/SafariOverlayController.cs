﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SafariOverlayController : MonoBehaviour {


	public GameObject plane1;
	public GameObject plane2;
	public GameObject plane3;

	public PicController pic1;
	public PicController pic2;
	public PicController pic3;

	public GameObject redicleMiddle; 

	Sprite s;
	bool safariZoom = false;

	void Update()
	{
		if (safariZoom) 
		{
			redicleMiddle.transform.Rotate (0f, 0f, 100f * Time.deltaTime);
		}
	}

	public void SetTextures(int index, Texture2D tex)
	{
		s = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0, 0));

		switch (index) 
		{
		case 1:
			plane1.GetComponent<Image>().sprite = s;
			break;
			
		case 2:
			plane2.GetComponent<Image>().sprite = s;
			break;
			
		case 3:
			plane3.GetComponent<Image>().sprite = s;
			break;
		}
	}

	public void ToggleSafariZoom()
	{
		safariZoom = !safariZoom;
	}


}
