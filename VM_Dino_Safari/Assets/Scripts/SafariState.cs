﻿using UnityEngine;
using System.Collections;

public class SafariState : MonoBehaviour {

	public RenderTexture rend1;
	public RenderTexture rend2;
	public RenderTexture rend3;

	public Texture2D texThumb1;
	public Texture2D texThumb2;
	public Texture2D texThumb3;

	public int picCount = 0;
	public int num1 = 99;
	public int num2 = 99;
	public int num3 = 99;
	
	public enum SAFARI_STATE
	{
		DEFAULT,
		SAFARI_MODE,
		POST_SAFARI_MODE
	}
	
	public SAFARI_STATE safariState;

	void Awake() {
		DontDestroyOnLoad(gameObject);
		
		texThumb1 = new Texture2D (256, 144, TextureFormat.RGB24, false);
		texThumb2 = new Texture2D (256, 144, TextureFormat.RGB24, false);
		texThumb3 = new Texture2D (256, 144, TextureFormat.RGB24, false);
		
		rend1 = new RenderTexture (1280, 720, 8, RenderTextureFormat.ARGB32);
		rend2 = new RenderTexture (1280, 720, 8, RenderTextureFormat.ARGB32);
		rend3 = new RenderTexture (1280, 720, 8, RenderTextureFormat.ARGB32);
	}

	void Start () {
	
	}

	public void GotoSafari() {
		safariState = SAFARI_STATE.SAFARI_MODE;
	}

	public void GotoSafariResults ()
	{
		safariState = SAFARI_STATE.POST_SAFARI_MODE;
	}
	

}
