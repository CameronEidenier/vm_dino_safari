﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//using DG.Tweening;

public class PicController : MonoBehaviour {

	public GameObject blank;
	public GameObject planeImg;
	public GameObject animalGood;
	public GameObject animalType;

	public Sprite goodImg;
	public Sprite badImg;
	public Sprite question;
	Sprite type;

	float animalAlpha;
	float goodBadAlpha;


	bool picTaken = false;

	public int index;
	
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		if (!picTaken) {

			if (goodBadAlpha > 0)
				goodBadAlpha -= .05f;

			if (animalAlpha < 1 && goodBadAlpha < 0.3) {
				planeImg.SetActive (false);
				animalAlpha += .05f;
			}

			animalGood.GetComponent<Image> ().color = new Vector4 (1, 1, 1, goodBadAlpha);
			planeImg.GetComponent<Image> ().color = new Vector4 (1, 1, 1, goodBadAlpha);
			animalType.GetComponent<Image> ().color = new Vector4 (1, 1, 1, animalAlpha);
		} 
		else 
		{
			if (goodBadAlpha > 0)
				goodBadAlpha -= .05f;

			planeImg.GetComponent<Image> ().color = new Vector4 (1, 1, 1, goodBadAlpha);

		}
	}

	public void VerifyCorrect()
	{
		animalGood.GetComponent<Image> ().sprite = goodImg;

		picTaken = true;
		
		animalGood.GetComponent<Image> ().color = new Vector4(1,1,1, 1);
		planeImg.GetComponent<Image> ().color = new Vector4 (1, 1, 1, 1);
		animalType.GetComponent<Image> ().color = new Vector4(1,1,1, 0);
	}

	public void VerifyWrong()
	{
		goodBadAlpha = 1.7f;
		animalAlpha = 0;
		animalGood.GetComponent<Image> ().sprite = badImg;

	}

	public void ResetPic()
	{
		picTaken = false;

		goodBadAlpha = 0;
		animalAlpha = 0;


		animalGood.GetComponent<Image> ().color = new Vector4 (1, 1, 1, goodBadAlpha);
		planeImg.GetComponent<Image> ().color = new Vector4 (1, 1, 1, goodBadAlpha);

		if (index != 1) {
			SetQuestion ();
		}
	}


	public void SetType()
	{
		animalType.GetComponent<Image> ().sprite = type;
	}

	public void SetType(Sprite s)
	{
		type = s;
		animalType.GetComponent<Image> ().sprite = type;
	}

	public void SetQuestion()
	{
		animalType.GetComponent<Image> ().sprite = question;
	}
}
