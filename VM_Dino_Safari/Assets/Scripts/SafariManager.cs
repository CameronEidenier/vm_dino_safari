//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine.UI;
//using System.IO;
//using System.Threading;
////using DG.Tweening;
////using Plate;
////using Wildlife;
//
//public class SafariManager : MonoBehaviour
//{
//	private Camera renderCamera;
//
//	public GameObject safariCameraPrefab;
//	public GameObject safariOverlayPrefab;
//	public GameObject safariResultsPrefab;
//
//	private GameObject safariCamera;
//	private GameObject safariOverlay;
//	private GameObject safariResults;
//
//	SafariOverlayController safariOverlayController;
//
//	private SafariState state;
//
////	protected AnimalUIController[] animals;
////	protected AnimalAbstractGOController[] animalSingles;
////	protected NodeController[] transitions;
//
//
////	public bool newlyLoaded = true;
////	public GameObject cameraMain;
//
////	bool zoomedIn = false;
//
////	public RenderTexture[] screenshots {
////		get {
////			return new RenderTexture[] { rend1};
////		}
////	}
//
////	public Sprite[] savannaSprites;
////	public Sprite[] amazonSprites;
////	public Sprite[] outbackSprites;
//
//	void Awake ()
//	{
////		animals = FindObjectsOfType (typeof(AnimalUIController)) as AnimalUIController[];
////		animalSingles = FindObjectsOfType (typeof(AnimalAbstractGOController)) as AnimalAbstractGOController[];
////		transitions = FindObjectsOfType (typeof(NodeController)) as NodeController[];
//
//		//gameObject.tag = "SceneController";
//
//	}
//
//	void Start() {
//		GrabFloorPlateListeners();
//		
//		state = FindObjectOfType<SafariState>();
//		if(state != null) {
//			
//			if(IsOnSafari())
//				ResumeSafari();
//		}
//	}
//
//	void OnDestroy() {
//		if(safariOverlay != null) {
//			safariOverlay.transform.SetParent(null);
//			Destroy(safariOverlay.gameObject);
//		}
//		if(safariCamera != null) {
//			safariCamera.transform.SetParent(null);
//			Destroy(safariCamera.gameObject);
//		}
//	}
//
////	void OnLevelWasLoaded (int level)
////	{
////		Debug.Log("[SafariManager] LevelLoaded");
////		GrabFloorPlateListeners();
////
////		if (newlyLoaded) { // only needed for Travel to Menagerie
////
////			animalSingles = FindObjectsOfType (typeof(AnimalAbstractGOController)) as AnimalAbstractGOController[];
////			animals = FindObjectsOfType (typeof(AnimalUIController)) as AnimalUIController[];
////			transitions = FindObjectsOfType (typeof(NodeController)) as NodeController[];
////
////			if(!TransitionScreen.instance.IsTransitioning)
////				TransitionScreen.instance.StartFadeIn();
////
////			newlyLoaded = false;
////		}
////	}
//
//	void GrabFloorPlateListeners ()
//	{
////		PlateAdapter plate = FindObjectOfType<PlateAdapter>();
////		if(plate == null) {
////			Debug.LogError("FloorPlateNotReadyYet");
////			return;
////		}
//
////		plate.MinigamePressed += OnFloorPlateSafariPressed;
////		plate.SelectLocationPressed += OnLeavingSafariAbleScene;
////		plate.HomePressed += OnLeavingSafariAbleScene;
//	}
//	
//	private void OnFloorPlateSafariPressed ()
//	{
////		SoundsPool.instance.PlayConfirm ();
//		ToggleSafari ();
//	}
//	
//	private void OnLeavingSafariAbleScene ()
//	{
//		ToDefault ();
//	}
//
//	public bool IsNormalMode ()
//	{
//		if(state == null)
//			return true;
//		return state.safariState == SafariState.SAFARI_STATE.DEFAULT;
//	}
//	
//	public bool IsOnSafari ()
//	{
//		if(state == null)
//			return false;
//		return state.safariState == SafariState.SAFARI_STATE.SAFARI_MODE;
//	}
//	
//	public bool IsOnSafariResults ()
//	{
//		if(state == null)
//			return false;
//		return state.safariState == SafariState.SAFARI_STATE.POST_SAFARI_MODE;
//	}
//
//	void getRenderImage (int index, Texture2D tex, RenderTexture rend)
//	{
//		renderCamera.targetTexture = rend;
//		renderCamera.Render ();
//
//		RenderTexture.active = rend;
//
//		//tex.ReadPixels(new Rect(0,0, rend.width, rend.height), 0, 0);
//		tex.ReadPixels (new Rect (500, 300, rend.width, rend.height), 0, 0);
//
//		RenderTexture.active = null;
//		tex.Apply ();
//
//		renderCamera.enabled = false;
//
//		safariOverlayController.SetTextures (index, tex);
//	}
//
//	// Update is called once per frame
//	void Update ()
//	{
//		Transform cameraMain = Camera.main.transform;
//		if (IsOnSafari())
//		{
////			Debug.DrawRay(cameraMain.position, cameraMain.forward * 100, Color.blue);
//			//Gizmos.DrawRay(cameraMain.position, cameraMain.forward * 100);
//
//			if (renderCamera.enabled)
//			{
//				renderCamera.enabled = false;
//			}
//			else if (Cardboard.SDK.Triggered)
//			{
//				bool CameraFail = false;
//
//				// if camera is looking at the floor plate
////				if (Cardboard.SDK.HeadPose.Orientation.eulerAngles.x > 45 && Cardboard.SDK.HeadPose.Orientation.eulerAngles.x < 90)
////				{
////					CameraFail = true;
////				}
//
//
////				RaycastHit[] hits = Physics.RaycastAll(cameraMain.position, cameraMain.forward, 200, LayerMask.GetMask("UI", "Default"));
//				RaycastHit[] hits = Physics.RaycastAll(cameraMain.position, cameraMain.forward, 200);
//
//				foreach(RaycastHit hit in hits) {
//					if(hit.transform.CompareTag("Tutorial"))
//						CameraFail = true;
//
//					// stops camera from triggering it another functionality is to happen
////					else if(hit.transform.GetComponent<NodeController>() != null)
////						CameraFail = true;
////
////					else if(hit.transform.GetComponent<AbstractNodeController>() != null)
////						CameraFail = true;
//
//					if(CameraFail)
//						break;
//				}
//
//
//				if (!CameraFail)
//				{
//					if (state.picCount < 3)
//					{
//						renderCamera.enabled = true;
//
//						if (state.picCount == 0)
//						{
//							getRenderImage (state.picCount + 1, state.texThumb1, state.rend1);
//							safariOverlayController.pic1.VerifyWrong ();
//							safariOverlayController.plane1.SetActive (true);
//						}
//						else if (state.picCount == 1)
//						{
//							getRenderImage (state.picCount + 1, state.texThumb2, state.rend2);
//							safariOverlayController.pic2.VerifyWrong ();
//							safariOverlayController.plane2.SetActive (true);
//						}
//						else if (state.picCount == 2)
//						{
//							getRenderImage (state.picCount + 1, state.texThumb3, state.rend3);
//							safariOverlayController.pic3.VerifyWrong ();
//							safariOverlayController.plane3.SetActive (true);
//						}
//
//						/*foreach (AnimalUIController animal in animals)
//						{
//							if (animal.lookedSafariAt)
//							{
//								if(picCount == 0 && animal.AnimalID == num1)
//								{
//									safariOverlayController.pic1.VerifyCorrect();
//									safariOverlayController.pic2.SetType();
//									picCount++;
//									renderCamera.targetTexture = rend2;
//								}
//								else if(picCount == 1 && animal.AnimalID == num2)
//								{
//									safariOverlayController.pic2.VerifyCorrect();
//									safariOverlayController.pic3.SetType();
//									picCount++;
//									renderCamera.targetTexture = rend3;
//								}
//								else if(picCount == 2 && animal.AnimalID == num3)
//								{
//									safariOverlayController.pic3.VerifyCorrect();
//									picCount++;
//								}
//							}
//						}*/
//
//						bool correctAnimal = false;
////						foreach (AnimalAbstractGOController animal in animalSingles) {
////							if (animal.IsLookedAtBy (cameraMain)) {
////
////								if (state.picCount == 0 && animal.animalUI.animalID == state.num1) {
////
////									safariOverlayController.pic1.VerifyCorrect ();
////									safariOverlayController.pic2.SetType ();
////									state.picCount++;
////									renderCamera.targetTexture = state.rend2;
////									correctAnimal = true;
////									break;
////								} else if (state.picCount == 1 && animal.animalUI.animalID == state.num2) {
////
////									safariOverlayController.pic2.VerifyCorrect ();
////									safariOverlayController.pic3.SetType ();
////									state.picCount++;
////									renderCamera.targetTexture = state.rend3;
////									correctAnimal = true;
////									break;
////								} else if (state.picCount == 2 && animal.animalUI.animalID == state.num3) {
////
////									safariOverlayController.pic3.VerifyCorrect ();
////									state.picCount++;
////									correctAnimal = true;
////									break;
////								}
////							}
////						}
//
////						if(correctAnimal)
////							SoundsPool.instance.PlaySafariGood();
////						else
////							SoundsPool.instance.PlaySafariBad();
//					}
//				}
//			}
//
//
////			foreach (AnimalAbstractGOController animal in animalSingles) {
////				if (animal.IsLookedAtBy (cameraMain)) {
////					animal.lookingAngle = animal.maxAngle;
////					animal.setSafariLookingAt (true);
////
////					if (!zoomedIn) {
////
//////						cameraMain.DOLocalMove (new Vector3 (0, 0, 5), 1, false);
//////						safariOverlay.transform.DOLocalMove (new Vector3 (0, 0, 5.341f), 1, false);
//////						safariOverlayController.ToggleSafariZoom();
//////						zoomedIn = true;
////						break;
////					}
////				} else {
////					animal.setSafariLookingAt (false);
////
////					if (zoomedIn && animal.lookingAngle == animal.maxAngle) {
////						animal.ResetLookingAngle ();
//////						cameraMain.DOLocalMove (new Vector3 (0, 0, 0), 1, false);
//////						safariOverlay.transform.DOLocalMove (new Vector3 (0, 0, 0.341f), 1, false);
//////						safariOverlayController.ToggleSafariZoom();
//////						zoomedIn = false;
////					}
////				}
////			}
//
//
//			// if 3 pictires have been taken
//			if (state.picCount >= 3 && !IsOnSafariResults()) {
//				ToPostSafari ();
//			}
//		}
//	}
//
//	public void ToggleSafari ()
//	{
//		if (IsNormalMode())
//			ToSafari ();
//		else if (!IsOnSafariResults())
//			ToDefault ();
//	}
//
//	public void SceneChangeNodeActivated ()
//	{
//		if(IsOnSafariResults()) {
//			ToDefault();
//		}
//	}
//
//	public void ToDefault ()
//	{
//		ToDefault(null);
//	}
//
//	public void ToDefault (GameObject b)
//	{
//		if(safariOverlay != null) {
//			safariOverlay.transform.SetParent(null);
//			Destroy(safariOverlay.gameObject);
//		}
//		if(safariCamera != null) {
//			safariCamera.transform.SetParent(null);
//			Destroy(safariCamera.gameObject);
//		}
//		if(safariResults != null) {
//			safariResults.transform.SetParent(null);
//			Destroy(safariResults.gameObject);
//		}
//
//		if(state != null)
//			Destroy(state.gameObject);
//
//		if(b != null) {
//			b.transform.SetParent(null);
//			Destroy (b.gameObject);
//		}
//	}
//
//	public void ToSafari ()
//	{
//		SetUpSafari ();
//	}
//
//	public void ToSafari (int firstID)
//	{
//		SetUpSafari ();
//	}
//
//	void SetUpSafari ()
//	{
//		SoundsPool.instance.PlaySafariStartSting();
//		TutorialManager.instance.SafariMoment();
//
//		// Create some objects...
//		GameObject stateObject = new GameObject("SafariState");
//		state = stateObject.AddComponent<SafariState>();
//		state.GotoSafari();
//		state.num1 = UnityEngine.Random.Range (0, 3);
//		state.num2 = UnityEngine.Random.Range (3, 6);
//		state.num3 = UnityEngine.Random.Range (6, 9);
//
//		ResumeSafari();
//	}
//
//	void ResumeSafari() 
//	{
//		CardboardHead head = FindObjectOfType<CardboardHead>();
//		
//		safariOverlay = GameObject.Instantiate(safariOverlayPrefab, head.transform.position, Quaternion.identity) as GameObject;
//		safariOverlay.transform.SetParent(head.transform);
//		safariOverlay.transform.localRotation = Quaternion.identity;
//		safariOverlay.transform.localPosition = Vector3.zero;
//		safariOverlay.transform.localScale = Vector3.one;
//		safariOverlayController = safariOverlay.GetComponentInChildren<SafariOverlayController> ();
//		//safariOverlay.SetActive(false);
//		
//		
//		safariCamera = GameObject.Instantiate(safariCameraPrefab, head.transform.position, Quaternion.identity) as GameObject;
//		safariCamera.transform.SetParent(head.transform);
//		safariCamera.transform.localRotation = Quaternion.identity;
//		safariCamera.transform.localPosition = Vector3.zero;
//		safariCamera.transform.localScale = Vector3.one;
//		renderCamera = safariCamera.GetComponent<Camera> ();
//
//		for (int i = 0; i < animals.Length; i++)
//			animals [i].OnSafariStarted ();
//
//		renderCamera.targetTexture = state.rend1;
//		
//		Debug.Log (state.num1 + " : " + state.num2 + " : " + state.num3);
//
//		Sprite[] sprites = null;
//
//		SafariBiome biome = FindObjectOfType<SafariBiome>();
//
//		if(biome.location == SafariLocation.Savanna)
//			sprites = savannaSprites;
//		else if(biome.location == SafariLocation.Amazon)
//			sprites = amazonSprites;
//		else if(biome.location == SafariLocation.Outback)
//			sprites = outbackSprites;
//
//		safariOverlayController.pic1.SetType (sprites [state.num1]);
//		safariOverlayController.pic2.SetType (sprites [state.num2]);
//		safariOverlayController.pic3.SetType (sprites [state.num3]);
//
//		if(state.picCount == 0)
//		{
//			safariOverlayController.pic1.ResetPic ();
//			safariOverlayController.pic2.ResetPic();
//			safariOverlayController.pic3.ResetPic();
//		}
//		else if(state.picCount == 1)
//		{
//			safariOverlayController.pic1.VerifyCorrect ();
//
//			safariOverlayController.pic2.ResetPic();
//			safariOverlayController.pic2.SetType();
//
//			safariOverlayController.pic3.ResetPic();
//		}
//		else if(state.picCount == 2)
//		{
//			safariOverlayController.pic1.VerifyCorrect ();
//			safariOverlayController.pic2.VerifyCorrect();
//
//			safariOverlayController.pic3.ResetPic();
//			safariOverlayController.pic3.SetType();
//		}
//	}
//
//	public void ToPostSafari ()
//	{
//		if(renderCamera != null) {
//			renderCamera.transform.SetParent(null);
//			Destroy(renderCamera.gameObject);
//		}
//
//		if(safariOverlay != null) {
//			safariOverlay.transform.SetParent(null);
//			Destroy(safariOverlay.gameObject);
//		}
//
//		float yrot = Cardboard.SDK.HeadPose.Orientation.eulerAngles.y;
//		Quaternion q = Quaternion.Euler(new Vector3(0f, yrot, 0f));
//
//		safariResults = Instantiate (safariResultsPrefab, Vector3.zero, q) as GameObject;
//
//		state.GotoSafariResults();
//
//		SoundsPool.instance.PlaySafariCompleteSting();
//	}
//
//	public RenderTexture GetRend (int index)
//	{
//		switch (index) {
//		case 1:
//			return state.rend1;
//
//		case 2:
//			return state.rend2;
//
//		case 3:
//			return state.rend3;
//		}
//
//		return null;
//	}
//}
