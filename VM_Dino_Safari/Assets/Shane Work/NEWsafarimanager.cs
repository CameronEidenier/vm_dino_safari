﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;
using System.Threading;

public class NEWsafarimanager : MonoBehaviour {

		GameObject cam ;
		private Camera renderCamera;

		SafariOverlayController safariOverlayController;

		CaptureAndSave snapshot;

		 RenderTexture rend1;
		 RenderTexture rend2;
		 RenderTexture rend3;

		 Texture2D texThumb1;
		 Texture2D texThumb2;
		 Texture2D texThumb3;
		Texture2D saveTex;
		 int picCount = 0;


	//Texture2D t = null;

		void Awake ()
		{
			texThumb1 = new Texture2D (256, 144, TextureFormat.RGB24, false);
			texThumb2 = new Texture2D (256, 144, TextureFormat.RGB24, false);
			texThumb3 = new Texture2D (256, 144, TextureFormat.RGB24, false);

			rend1 = new RenderTexture (1280, 720, 8, RenderTextureFormat.ARGB32);
			rend2 = new RenderTexture (1280, 720, 8, RenderTextureFormat.ARGB32);
			rend3 = new RenderTexture (1280, 720, 8, RenderTextureFormat.ARGB32);


			

			cam = new GameObject();
			cam.transform.parent = gameObject.transform;
		cam.transform.position = gameObject.transform.position;
		cam.transform.rotation = gameObject.transform.rotation;
			// instantiate cam to this object's position and rotation (the mono camera of cardboard)
		//cam = Instantiate(new GameObject(), transform.position, transform.rotation);
			cam.AddComponent<Camera> ();	
			renderCamera = cam.GetComponent<Camera> ();
			renderCamera.depth = -100;
			renderCamera.enabled = false;




		}
		
		void Start(){
			//Instantiate(cam, transform.position, transform.rotation);
		saveTex = new Texture2D(1280,720, TextureFormat.RGB24, false);

		snapshot = GameObject.FindObjectOfType<CaptureAndSave> ();
		}

		void OnDestroy() {
//			if(safariOverlay != null) {
//				safariOverlay.transform.SetParent(null);
//				Destroy(safariOverlay.gameObject);
//			}
//			if(safariCamera != null) {
//				safariCamera.transform.SetParent(null);
//				Destroy(safariCamera.gameObject);
//			}
		}
		

		void getRenderImage (int index, Texture2D tex, RenderTexture rend)
		{
			renderCamera.targetTexture = rend;
			renderCamera.Render ();

			RenderTexture.active = rend;

			tex.ReadPixels (new Rect (500, 300, rend.width, rend.height), 0, 0);
			tex.Apply ();

			////////////////////////
//		if(t != null)
//		{
//			print ("innnnnnnn save");
//			snapshot.SaveTextureToGallery (t);
//		}
			////////////////////////
			SaveRenderTexture(rend);
            RenderTexture.active = null;
			//tex.Apply ();



			renderCamera.enabled = false;
		print ("made itar here");
			

        //safariOverlayController.SetTextures (index, tex);
            

        GameObject SafariPic = (GameObject)Instantiate ((GameObject)Resources.Load("SafariPic", typeof(GameObject)), Vector3.zero, Quaternion.Euler(new Vector3(0 ,0 ,0)));

		GameObject.Find("FrontPlane").GetComponent<Renderer> ().material.mainTexture = GetRend(1);


			picCount++;
		}

		// Update is called once per frame
		void Update ()
		{
			Transform cameraMain = Camera.main.transform;

				if (renderCamera.enabled)
				{
					renderCamera.enabled = false;
				}
				else if (picCount < 1 && Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1))//(Cardboard.SDK.Triggered)
				{
							print ("IN");
							renderCamera.enabled = true;

							if (picCount == 0)
							{
								getRenderImage (picCount + 1, texThumb1, rend1);
                                
							}
							else if (picCount == 1)
							{
								getRenderImage (picCount + 1, texThumb2, rend2);
							}
							else if (picCount == 2)
							{
								getRenderImage (picCount + 1, texThumb3, rend3);
							}
					}
				

				// if 3 pictires have been successfully taken
				if (picCount >= 3) {
					
				}
			
		}

		public RenderTexture GetRend (int index)
		{
			switch (index) {
			case 1:
				return rend1;

			case 2:
				return rend2;

			case 3:
				return rend3;
			}

			return null;
		}
	public void SaveRenderTexture(RenderTexture rend)
        {
        int hight = 1280;
        int width = 720;

		RenderTexture.active = rend;
        //print("Saving Image");
		//Texture2D saveTex = new Texture2D(hight, width, TextureFormat.RGB24, false);
		print("Made new texture 2d");
        saveTex.ReadPixels(new Rect(0, 0, hight, width), 0, 0);
		saveTex.Apply ();
		print ("Read Pixles");
        //byte[] bytes = saveTex.EncodeToPNG();

        if (Application.platform == RuntimePlatform.Android)
        {
            //File.WriteAllBytes(Application.persistentDataPath + "/screenshot.png", bytes);
        }
        else if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
			snapshot.SaveTextureToGallery (saveTex);
        }
        else
        {
            //File.WriteAllBytes(Application.dataPath + "/../screenshot.pngs", bytes);
        }
        print("Done Saving");
        }
	}

